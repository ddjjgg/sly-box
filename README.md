# sly-box


(UNDER CONSTRUCTION)

This library will provide (an attempt at) generic container data types in C.

There is currently no work here yet, but there will be soon. To get an idea
about what will be here, view the #SK_BoxIntf data structure in the 'spek-net'
project. The code associated with that data stucture will be moved here, likely
with name-changes and many other refinements.

In essence, #SK_BoxIntf is an interface for "storage data structures" that
can be accessed like an array, but are not necessarily implemented as a normal
array. Here, being accessed "like an array" means that the indices for
elements are contiguous, not that the elements are contiguously packed in
memory. I.e. if the storage object contains N elements, then indices [0, N - 1]
all contain accessible elements.

This allows for functions to be written against #SK_BoxIntf, treating the
resulting storage object like an array, but without actually caring about the
storage object's memory layout. A caller of such a function could then provide
an #SK_BoxIntf to the function as an argument, which allows for different
container types to be used transparently.

The benefit here is that different container types have different run-time
complexities for different operations. So, for example, if quick element
accesses are prioritized, then an #SK_BoxIntf that is actually implemented as a
regular array could be provided. If quick resizes are prioritized, then an
 #SK_BoxIntf that is implemented as a vector could be provided. If quick
insertions / removals are prioritized, then an #SK_BoxIntf that is implemented
as a linked-list could be provided. Etc., etc., etc.

Immediately, such an interface should raise some efficiency concerns. Consider
that an access pattern intended for an array is not necessarily efficient when
performed with a linked-list. Taking into account cache locality, array
accesses are generally quick as long as the current access's index is close to
a previous access's index. However, linked-list accesses incur many
cache-misses, and only get slower as the element indices get further from the
start of the list.

For this reason, "anchors" are provided by #SK_BoxIntf. An anchor is a data
type that the #SK_BoxIntf implementation dictates the size of; a size which the
user of the #SK_BoxIntf can optionally allocate. Anchors allow #SK_BoxIntf
implementations to store whatever data is needed to access a given element
index again quickly (and possibly the surrounding indices too).

For example, to access a given element in a linked-list faster than "walking
the list", you may need to store a pointer directly to the desired element.
Additionally, to make accessing "nearby" indices quicker, it may be
desirable to store the index of that element and pointers to the previous /
next blocks of the linked-list. So, an #SK_BoxIntf implemented as a linked-list
could then report the size of anchors as being the size of 1 index and 3
pointers (in practice, (sizeof (size_t) + (3 * sizeof (void *))) ).

When accessing an element, users of #SK_BoxIntf can optionally provide
a pointer to where an anchor can be stored. By again passing this anchor the
next time any nearby elements are accessed, the access can be made much
quicker than without the use of the anchor. In this manner, anchors provide the
property of "nearby-accesses-are-quick" to arbitrary container types. In turn,
access patterns that are reasonable for arrays could at least be made
not-terrible for other container types.

Does any of this actually work in practice? I don't know. And even if it
"works", is it ergonomic to use? I don't know that either.

A lot more testing / refinement in general is needed for #SK_BoxIntf. That's
true not just with anchors, but with basically everything about it. Is the
interface described by #SK_BoxIntf feature-complete? Are the hints communicated
in the best way possible? Is the set of provided hints reasonable? Will the
design of #SK_BoxIntf subtly cause inefficiencies later on? Will #SK_BoxIntf
ever compile efficiently considering this is all done without templates?
Particularly, will #SK_BoxIntf perform nicely with link-time optimization?

With all this said, work related to #SK_BoxIntf will be moved here from
spek-net at the first convenient moment.




